#include <stdio.h>

#include "espressif/esp_common.h"
#include "esp/uart.h"

#include "FreeRTOS.h"
#include "task.h"

#include "i2c/i2c.h"
#include "bmp280/bmp280.h"

// In forced mode user initiate measurement each time.
// In normal mode measurement is done continuously with specified standby time.
#define MODE_FORCED 1
const uint8_t i2c_bus = 0;
const uint8_t scl_pin = 14;
const uint8_t sda_pin = 12;


// Check it yourself
static void test_leds(void *pvParameters)
{
	uint8_t pcf_address = 0x38;
	uint8_t input = 0x0f;
			// Write Pcf address
	bool ack;
	//ack = i2c_write(i2c_bus,pcf_address);

	while(1)
	{
		i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
		printf("Begin");
		i2c_slave_read(i2c_bus, 0x38, NULL, &input, 1);
		vTaskDelay(10 / portTICK_PERIOD_MS); // sleep 100ms
		input = ~(input >> 4);
		i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
	}
}



// Running light 
static void light_show(void *pvParameters)
{
	uint8_t pcf_address = 0x38;
	uint8_t input = 0x0f;
			// Write Pcf address
	bool ack;
	//ack = i2c_write(i2c_bus,pcf_address);
	i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
	vTaskDelay(100 / portTICK_PERIOD_MS); // sleep 100ms
	input = 0x0f;
	i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
	vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms

	while(1)
	{

		input = 0x0e;
		i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
		vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms

		input = 0x0d;
	    i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
		vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms

	    input = 0x0b;
	    i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
		vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms

	    input = 0x07;
	    i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
		vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms

		 input = 0x0b;
		i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
		vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms

		input = 0x0d;
		i2c_slave_write(i2c_bus, 0x38, NULL, &input, 1);
		vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms




	}
}


void user_init(void)
{
    uart_set_baud(0, 115200);
    int succ ;
    // Just some information
    printf("\n");
    printf("SDK version : %s\n", sdk_system_get_sdk_version());
 //   printf("GIT version : %s\n", GITSHORTREV);

    i2c_init(i2c_bus, scl_pin, sda_pin, I2C_FREQ_400K);


    // Just some information
    printf("\n");
    printf("SDK version : %s\n", sdk_system_get_sdk_version());
    xTaskCreate(light_show, "bmp280_task", 256, NULL, 2, NULL);

}
