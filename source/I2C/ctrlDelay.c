/*
 * ctrlDelay.c
 *
 *  Created on: Feb 26, 2018
 *      Author: bso
 */


/* 
 * Respond to a onboard button pressing.
 * by changing frequency of led blinking
 *  
 */

#include <stdbool.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "esp8266.h"
#include <espressif/sdk_private.h>
#include "i2c/i2c.h"

// I2c parameters
const uint8_t i2c_bus = 0;
const uint8_t scl_pin = 14;
const uint8_t sda_pin = 12;
const int gpio = 2;




void ctrlDelay (void *pvParameters){

   uint8_t mask = 0xf0; // serves for masking outputs
   uint8_t curr_value; // current value
   uint8_t old_value; // when buttons are opened output is equal to  1
   old_value = 0xf0;
   uint delay =  2; // Info about delay in microseconds, Default value
   uint8_t input;
   uint8_t error_input;
   uint8_t init = 0xff;
   i2c_slave_write(i2c_bus, 0x38, NULL, &init, 1);
   while (1)
   {
	   // Read output
		i2c_slave_read(i2c_bus, 0x38, NULL, &input, 1);

		// mask the port of leds
		curr_value = mask & input;
		printf("curr value %#04x\n \n",curr_value);

		// Check if there was change
		// and latch the state
		if (curr_value != old_value){
			printf("curr value %d\n \n",15);
			switch (curr_value)
			{
				 case 0xe0:
				 //state 1110
					  delay = 500;
					  break;
				 case 0xd0:
				 //state 1101
					  delay = 50;
					  break;
				 //state 1101
				 case 0xb0:
					  delay = 25;
					  break;

				 case 0x70:
					 delay = 5;
					 break;
				 default:
				 // little easter egg

					error_input = 0xf0;
					i2c_slave_write(i2c_bus, 0x38, NULL, &error_input, 1);
					break;
			}
		}

		TickType_t xDelay = delay/portTICK_PERIOD_MS;
		vTaskDelay( xDelay );
		gpio_write(gpio,1);
		vTaskDelay( xDelay );
		gpio_write(gpio,0);
   }

}

void user_init(void)
{
    uart_set_baud(0, 115200);
    gpio_enable(gpio, GPIO_OUTPUT);
    // Don't forget all init stuff goes in user_init part
    i2c_init(i2c_bus, scl_pin, sda_pin, I2C_FREQ_400K);


    // Just some information
    printf("\n");
    printf("SDK version : %s\n", sdk_system_get_sdk_version());
    xTaskCreate(ctrlDelay, "ctrlDelay", 256, NULL, 2, NULL);

}
