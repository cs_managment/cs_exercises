/*
 * net_basics.c
 *
 *  Created on: April 29, 2018
 *      Author: ratko
 */


/*
 *
 * Introductory program for ESP8266 networking
 *
 */

#include "espressif/esp_common.h"
#include "esp/iomux.h"
#include "esp/uart.h"
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include <lwip/sockets.h>
#include "espressif/user_interface.h"
#include <lwip/dhcp.h>




#define FREQ_DIVIDER 0


#define WIFI_LED_IO_MUX     PERIPHS_IO_MUX_GPIO2_U
#define WIFI_LED_IO_NUM     2
#define WIFI_LED_IO_FUNC    FUNC_GPIO2


/* pin config */
const int gpio = 2;   /* gpio 0 usually has "PROGRAM" button attached */
const int active = 1; /* active == 0 for active low */

// Handler for controlling the task blinkenTask
TaskHandle_t turn_on,turn_off;


// Callback function for UDP

udp_recv_fn nefastor_udp_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, struct ip_addr *addr, u16_t port)
{
  if (p != NULL)
  {
   uint8_t data = (uint8_t) ((uint8_t*)p->payload)[0];

   pbuf_free(p);

   if (data == 1){
     vTaskResume (turn_on);
     vTaskSuspend (turn_off);
   }
   if (data == 0){
	   vTaskResume (turn_off);
	   vTaskSuspend (turn_on);
   }
  }
}


void turn_on_task(void *pvParameters)
{
    gpio_enable(gpio, GPIO_OUTPUT);
    while(1) {
        gpio_write(gpio, 0);
    }
}

void turn_off_task(void *pvParameters)
{
    gpio_enable(gpio, GPIO_OUTPUT);
    while(1) {
        gpio_write(gpio, 1);
    }
}


// Callback function for UDP


void user_init(void)
{

    uart_set_baud(0, 115200);
    xTaskCreate(turn_on_task, "turn_on", 256, NULL, 2,  &turn_on);
    xTaskCreate(turn_off_task, "turn_off", 256, NULL, 2,  &turn_off);
    vTaskSuspend (turn_off);
    vTaskSuspend (turn_on);


    // set params for network
    char ssid[] = "test";
    char password[] = "ratkoratko";
    struct sdk_station_config _station_info;

    strcpy((char *)_station_info.ssid, ssid);
    strcpy((char *)_station_info.password, password);
    _station_info.bssid_set = 0;

    //Must call sdk_wifi_set_opmode before sdk_wifi_station_set_config
    sdk_wifi_set_opmode(STATION_MODE);
    sdk_wifi_station_set_config(&_station_info);

    // setting static IP adress
    struct ip_info info;
    sdk_wifi_station_dhcpc_stop();
	IP4_ADDR(&info.ip, 192, 168, 43, 200);
	IP4_ADDR(&info.gw, 192, 168, 43, 1);
	IP4_ADDR(&info.netmask, 255, 255, 255, 0);
	sdk_wifi_set_ip_info(STATION_IF, &info);

    // Connect esp8266 to a network
    sdk_wifi_station_connect();
	//_is_esp_connected_to_wifi();

    // Creating task and parsing handler

    // Listening for UDP
    struct udp_pcb *nefastor_pcb;
    nefastor_pcb = udp_new();
    udp_bind(nefastor_pcb, IP_ADDR_ANY, 8888);
    udp_recv(nefastor_pcb, nefastor_udp_recv, NULL);


}
