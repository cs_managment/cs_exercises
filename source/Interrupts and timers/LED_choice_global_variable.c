/*
 * ctrlDelay.c
 *
 *  Created on: Feb 26, 2018
 *      Author: bso
 */


/* 
 * Changels the LED_choice value when button is pressed
 * and prints it on output
 *  
 */

#include <stdbool.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "esp8266.h"
#include <espressif/sdk_private.h>
#include "i2c/i2c.h"

// I2c parameters
const uint8_t i2c_bus = 0;
const uint8_t scl_pin = 14;
const uint8_t sda_pin = 12;
const int gpio = 0;   /* gpio 0 usually has "PROGRAM" button attached */
const int active = 0; /* active == 0 for active low */
const gpio_inttype_t int_type = GPIO_INTTYPE_EDGE_NEG;

volatile uint8_t LED_select = -1; // Globable variable that contains LED status


void gpio_intr_handler()
{
	// Inside ISR, we increment LED_select
	// This will only happen when ISR is run
	// ISR is run when interrupt happens
	// strictly HW thing. Don't call ISR from main program

	uint8_t tmp;
	tmp = LED_select + 1;
	LED_select = tmp % 4;
}

void main_Task(void *pvParameters)
{

	// Setting interrupt and attaching ISR

	gpio_set_interrupt(gpio, int_type, gpio_intr_handler);
    while(1) {
        printf("Pressed button %d \n",LED_select);

    }
}


void user_init(void)
{
    uart_set_baud(0, 115200);

    // Enable as gpio as input
    gpio_enable(gpio, GPIO_INPUT);
    // Don't forget all init stuff goes in user_init part
    i2c_init(i2c_bus, scl_pin, sda_pin, I2C_FREQ_400K);



    // Just some information
    printf("\n");
    printf("SDK version : %s\n", sdk_system_get_sdk_version());
    xTaskCreate(main_Task, "main_Task", 256, NULL, 2, NULL);

}

