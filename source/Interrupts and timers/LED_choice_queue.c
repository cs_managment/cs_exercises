/*
 * ctrlDelay.c
 *
 *  Created on: Feb 26, 2018
 *      Author: bso
 */


/* 
 * Changes the LED_choice value when button is pressed
 * and prints it on output
 * Done using queues
 */

#include <stdbool.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "esp8266.h"
#include <espressif/sdk_private.h>
#include "i2c/i2c.h"

// Include this library for queues
#include "queue.h"
// I2c parameters
const uint8_t i2c_bus = 0;
const uint8_t scl_pin = 14;
const uint8_t sda_pin = 12;
const int gpio = 0;   /* gpio 0 usually has "PROGRAM" button attached */
const int active = 0; /* active == 0 for active low */
const gpio_inttype_t int_type = GPIO_INTTYPE_EDGE_NEG;

volatile uint8_t LED_select = -1; // Globable variable that contains LED status


// Struct that handles two queues
typedef struct
{
	QueueHandle_t ISR_read_queue; // Queue that is read from a ISR but written by a main program
	QueueHandle_t ISR_write_queue; // Queue that is written by a ISR but read by a main program
} xQueues;


xQueues ISR_queues;


void gpio_intr_handler()
{
	// Inside ISR, we increment LED_select
	// This will only happen when ISR is run
	// ISR is run when interrupt happens
	// strictly HW thing. Don't call ISR from main program

	uint8_t tmp;
	uint8_t LED_select;

	// Read from ISR_read_queue to obtain previous LED choice
	xQueueReceiveFromISR(ISR_queues.ISR_read_queue, &tmp, NULL);

	// Increment it and do module
	tmp++;
	LED_select = tmp % 4;

	// Write to  ISR_write_queue to obtain send next LED choice to the main program
	xQueueSendToBackFromISR(ISR_queues.ISR_write_queue, &LED_select, NULL);

	tmp = LED_select + 1;
	LED_select = tmp % 4;
}

void main_Task(void *pvParameters)
{

	uint8_t LED_select_old = 255;
	uint8_t LED_select_curr = LED_select_old;

	// Setting interrupt and attaching ISR
	gpio_set_interrupt(gpio, int_type, gpio_intr_handler);

	// Set the queues
	xQueues *ISR_queues = (xQueues *)pvParameters;

	// Initialize ISR_read_queue
	// This is bit tricky part because use of pointers
	xQueueSendToBack((ISR_queues->ISR_read_queue),&LED_select_old, portMAX_DELAY);


    while(1) {



    	// read from a ISR_write_queue
    	xQueueReceive((ISR_queues->ISR_write_queue),&LED_select_curr, 20);
        printf("Pressed button %d \n",LED_select_curr);
        // write the old value to ISR_read_queue
        LED_select_old = LED_select_curr;
        xQueueSendToBack((ISR_queues->ISR_read_queue),&LED_select_old, 20);
    }

}



void user_init(void)
{
    uart_set_baud(0, 115200);

    //  Create ISR_write_queue
    ISR_queues.ISR_write_queue = xQueueCreate(2, sizeof(uint8_t));
    //  Create ISR_read_queue
    ISR_queues.ISR_read_queue = xQueueCreate(2, sizeof(uint8_t));

    // Enable as gpio as input
    gpio_enable(gpio, GPIO_INPUT);
    // Don't forget all init stuff goes in user_init part
    i2c_init(i2c_bus, scl_pin, sda_pin, I2C_FREQ_400K);



    // Just some information
    printf("\n");
    printf("SDK version : %s\n", sdk_system_get_sdk_version());

    // Pass the queues to the main task
    xTaskCreate(main_Task, "main_Task", 256, &ISR_queues, 2, NULL);

}

