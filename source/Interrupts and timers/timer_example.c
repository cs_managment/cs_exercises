/*
 * timer_pwm.c
 *
 *  Created on: Feb 26, 2018
 *      Author: bso
 */


/* Respond to a button press.
 *
 * This code combines two ways of checking for a button press -
 * busy polling (the bad way) and button interrupt (the good way).
 *
 * This sample code is in the public domain.
 */
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "esp8266.h"
#include <espressif/sdk_private.h>
#include <stdbool.h>
#define FREQ_DIVIDER 0


/* pin config */
const int gpio = 2;   /* gpio 0 usually has "PROGRAM" button attached */
const int active = 1; /* active == 0 for active low */
const gpio_inttype_t int_type = GPIO_INTTYPE_EDGE_NEG;
static uint32_t end_value = 1500;
static bool my_stat = false;


static void IRAM frc1_interrupt_handler(void *arg){
	// Change stat
	my_stat = !my_stat;
    gpio_write(gpio,my_stat);

	// Reinitial timer

    timer_set_load(FRC1,end_value);
    timer_set_run(FRC1,true);

	// Write value
   // printf(" %d \n",end_value);

}

void user_init(void)
{
    uart_set_baud(0, 115200);
    // Set selected gpio to be output
    gpio_enable(gpio, GPIO_OUTPUT);
    gpio_write(gpio, false);

    // Calculate frequency to count
    end_value = timer_freq_to_count(FRC1,100,FREQ_DIVIDER);
    if(timer_freq_to_count(FRC1,1,FREQ_DIVIDER) < 0){
    	my_stat = true;

    }
    gpio_write(gpio, my_stat);


    // Setting up timer
    // a) load the value
    timer_set_load(FRC1,end_value);
    // b) Enable interrupt
    timer_set_interrupts(FRC1, true);
    // c) attach interrupt
    _xt_isr_attach(INUM_TIMER_FRC1, frc1_interrupt_handler, NULL);
    // d) Turn on auto-reload
    timer_set_reload(FRC1,false);
    // f) Set frequency divider
    timer_set_divider(FRC1,FREQ_DIVIDER);
    // a) Enable timer

    timer_set_run(FRC1,true);

    printf("Main: %d \n",timer_max_load(FRC1));


}