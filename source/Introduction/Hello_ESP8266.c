/*
 * Hello_ESP8266.c
 *
 *  Created on: Feb 26, 2018
 *      Author: bso
 */


/* Two tasks 
 *
 * Each task prints its own string
 * Example of multithreading in FreeRTOS
 */

#include <stdbool.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "esp8266.h"
#include <espressif/sdk_private.h>
#include <stdbool.h>
#include "lwip/udp.h"


// First task that prints string
void vTask1Function (void *pvParameters){

   while (1)
   {
	printf("Hello from the task1 \n");
   }

}

// Second task that prints string
void vTask2Function (void *pvParameters){

   while (1)
   {
	printf("Hello from the task2 \n");
   }

}

void user_init(void)
{

	// Init serial port 0. Port 0 is used for communication between host and ESP8266 
	// First argument port number, second argument Baud rate (Symbols per second)
    uart_set_baud(0, 115200);
	// Create task. Look closely the parameters
    xTaskCreate( vTask1Function,"Task1",256,NULL,2,NULL);
    xTaskCreate( vTask2Function,"Task2",256,NULL,2,NULL);
}
