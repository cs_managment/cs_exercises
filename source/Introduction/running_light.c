/* Running light example
 *
 * This sample code is in the public domain.
 */
#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "esp8266.h"

// Selection of GPIOs could be different
// As long they are connected to appropriate leds to the board
const int gpio0 = 2;
const int gpio1 = 0;
const int gpio2 = 4;
const int gpio3 = 5;

/* This task uses the high level GPIO API (esp_gpio.h) to set GPIO pins.
 * by this order
 * 1000 -> 0100 -> 0010 -> 0001 -> 1000 -> ..... 
 */
void runningLight(void *pvParameters)
{
	// Enable  GPIOs as OUTPUTS
    gpio_enable(gpio0, GPIO_OUTPUT);
	gpio_enable(gpio1, GPIO_OUTPUT);
	gpio_enable(gpio2, GPIO_OUTPUT);
	gpio_enable(gpio3, GPIO_OUTPUT);
    while(1) {
		// *---
		// Writing gpios
        gpio_write(gpio0, 1);
		gpio_write(gpio1, 0);
		gpio_write(gpio2, 0);
		gpio_write(gpio3, 0);
		// Pause
        vTaskDelay(1000 / portTICK_PERIOD_MS);
		//-*--
        // Writing gpios
        gpio_write(gpio0, 0);
		gpio_write(gpio1, 1);
		gpio_write(gpio2, 0);
		gpio_write(gpio3, 0);
		// Pause
        vTaskDelay(1000 / portTICK_PERIOD_MS);
		//--*-
        // Writing gpios
        gpio_write(gpio0, 0);
		gpio_write(gpio1, 0);
		gpio_write(gpio2, 1);
		gpio_write(gpio3, 0);
		// Pause
        vTaskDelay(1000 / portTICK_PERIOD_MS);
		//---*
        // Writing gpios
        gpio_write(gpio0, 0);
		gpio_write(gpio1, 0);
		gpio_write(gpio2, 0);
		gpio_write(gpio3, 1);
		// Pause
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void user_init(void)
{
    uart_set_baud(0, 115200);
    xTaskCreate(runningLight, "RunningLight", 256, NULL, 2, NULL);
    //xTaskCreate(blinkenRegisterTask, "blinkenRegisterTask", 256, NULL, 2, NULL);
}