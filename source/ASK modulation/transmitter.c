#include "string.h"
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "i2c/i2c.h"
#include "ssid_config.h"
#include "ota-tftp.h"

#define PCF_ADDRESS	0x38
#define BUS_I2C		0
#define SCL 		14
#define SDA 		12

#define button1		0x20	// 0b ??0? ????
#define led1 		0xfe	// 0b ???? ???0

#define gpio_tx 	4		// D2 pin
#define gpio_rx 	5		// D1 pin

#define time_interval	500

static const int tx_interval = time_interval;
static TaskHandle_t pcf_task_handle, syn115_task_handle;
static TickType_t last_wake;

// toggle led1, delay for tx_interval and write value to gpio_tx
void write_tx(uint8_t value) {
	uint8_t pcf_byte;

	i2c_slave_read(BUS_I2C, PCF_ADDRESS, NULL, &pcf_byte, 1);
	pcf_byte = (pcf_byte ^ ~led1) | led1;
	i2c_slave_write(BUS_I2C, PCF_ADDRESS, NULL, &pcf_byte, 1);
	vTaskDelayUntil(&last_wake, pdMS_TO_TICKS(tx_interval));
	gpio_write(gpio_tx, value);
}

// transmit data
void syn115_task(void *pvParameters) {
	static int i;

	while (1) {

		// suspend pcf task
		vTaskSuspend(pcf_task_handle);
		last_wake = xTaskGetTickCount();

		// transmit [10] 8 times
		for (i = 0; i < 8; i++) {
			write_tx(1);
			write_tx(0);
		}

		// resume pcf task and suspend itself
		vTaskResume(pcf_task_handle);
		vTaskSuspend(NULL);
	}
}

// check for pressed button
void pcf_task(void *pvParameters) {
	uint8_t pcf_byte;

	while (1) {

		i2c_slave_read(BUS_I2C, PCF_ADDRESS, NULL, &pcf_byte, 1);
		// button 1 is pressed
		if ((pcf_byte & button1) == 0)
			// resume syn115 task
			vTaskResume(syn115_task_handle);
		// check again after 200 ms
		vTaskDelay(pdMS_TO_TICKS(200));
	}
}

void user_init(void) {

	uart_set_baud(0, 115200);
	i2c_init(BUS_I2C, SCL, SDA, I2C_FREQ_100K);
	gpio_enable(SCL, GPIO_OUTPUT);

	

	// set tx pin as output
	gpio_enable(gpio_tx, GPIO_OUTPUT);

	// create pcf task
	xTaskCreate(pcf_task, "PCF task", 1000, NULL, 2, &pcf_task_handle);

	// create and suspend syn115 task
	xTaskCreate(syn115_task, "SYN115 transmit task", 1000, NULL, 3, &syn115_task_handle);
	vTaskSuspend(syn115_task_handle);
}