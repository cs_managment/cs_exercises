/*
 * ctrlDelay.c
 *
 *  Created on: April 29,2018
 *      Author: ratko
 */


/* 
 * Reading MPU9250 together with calibration
 *
 *
 */

#include <stdbool.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "esp8266.h"
#include <espressif/sdk_private.h>
#include "i2c/i2c.h"

// Include this library for queues
#include "queue.h"
// I2c parameters
const uint8_t i2c_bus = 0;
const uint8_t scl_pin = 14;
const uint8_t sda_pin = 12;
const uint8_t mpu9250_adress = 0x68;

// Global variable needed for calibration look mpu9250_calib()
int x_acc_sum = 0;
int y_acc_sum = 0;
int z_acc_sum = 0;

void mpu9250_init(){
	uint8_t pwr_mng_address = 0x6c;
	uint8_t acc_conf0_address = 0x1c;
	uint8_t acc_conf1_address = 0x1d;
	uint8_t pwr_mng_payload = 0x00;
	uint8_t acc_conf0_payload = 0x00;
	uint8_t acc_conf1_payload = 0x05;


	i2c_slave_write(i2c_bus, mpu9250_adress, &pwr_mng_address, &pwr_mng_payload, 1);
	i2c_slave_write(i2c_bus, mpu9250_adress, &acc_conf0_address, &acc_conf0_payload, 1);
	i2c_slave_write(i2c_bus, mpu9250_adress, &acc_conf1_address, &acc_conf1_payload, 1);


}


void mpu9250_read_acc(void *pvParameters){
	// Readings are in second complement
	// e.g.
	// -2g -> -2^15
	// 2g -> 2^15 - 1

	uint8_t data_start_address = 0x3b;
	uint8_t data[6];
	uint8_t i;
	short x_acc = 0;  // in 2's complement
	short y_acc = 0;
	short z_acc = 0;
	float x_acc_g = 0; // acceleration in g units
	float y_acc_g = 0;
	float z_acc_g = 0;
	while(1){

		data_start_address = 0x3b;

		for (i=0;i<6;i++){

			i2c_slave_read(i2c_bus, mpu9250_adress, &data_start_address, &data[i], 1);
			data_start_address++;
		}

		x_acc = data[1] + ((short)data[0] << 8);
		y_acc = data[3] + ((short)data[2] << 8);
		z_acc = data[5] + ((short)data[4] << 8);



		x_acc_g = (float)(x_acc - (short)x_acc_sum) / (1 << 14); // 2^14
		y_acc_g = (float)(y_acc - (short)y_acc_sum) / (1 << 14); // 2^14
		z_acc_g = (float)(z_acc - (short)z_acc_sum) / (1 << 14); // 2^14

		printf("X_acc [g]: %1.5f \n",(x_acc_g));
		printf("Y_acc [g]: %1.5f \n",(y_acc_g));
		printf("Z_acc [g]: %1.5f \n",(z_acc_g));
		vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms




	}
}

void mpu9250_calib(){

	// collect 100 measurements and average it
	// This is the template for calibration, but it isn't needed
	// When the Gy-91 is still, then you have acc_z = 1g and acc_x = acc_y = 0
	// in other words readings from sensor are acc_z_out = 2^14, acc_x_out = acc_x_out \approx 0
	// After finding mean you need also to store calibration value in X,Y and Z_offs_register
	// but implemented here with global variable



	uint8_t data_start_address = 0x3b;
	uint8_t data[6];
	uint8_t i,j;


			for(i=0;i<100;i++){

				data_start_address = 0x3b;

				for (j=0;j<6;j++){

					i2c_slave_read(i2c_bus, mpu9250_adress, &data_start_address, &data[j], 1);
					data_start_address++;

				}

				x_acc_sum += data[1] + ((int)data[0] << 8);
				y_acc_sum += data[3] + ((int)data[2] << 8);
				z_acc_sum += data[5] + ((int)data[4] << 8);

				}

			x_acc_sum /= 100;
			y_acc_sum /= 100;
			z_acc_sum /= 100;
			z_acc_sum = z_acc_sum - (1<<14) ;  // when standing, sensors measures 1g in z-axis and that corrensponds to 2^14

}

void user_init(){

	i2c_init(i2c_bus, scl_pin, sda_pin, I2C_FREQ_400K);

	uart_set_baud(0, 115200);
	// Just some information
	printf("\n");
	printf("SDK version : %s\n", sdk_system_get_sdk_version());
    //   printf("GIT version : %s\n", GITSHORTREV);

	i2c_init(i2c_bus, scl_pin, sda_pin, I2C_FREQ_400K);

	mpu9250_init();
	vTaskDelay(500 / portTICK_PERIOD_MS); // sleep 100ms

	mpu9250_calib();
	vTaskDelay(1000 / portTICK_PERIOD_MS); // sleep 100ms

	// Just some information
	printf("\n");
	printf("SDK version : %s\n", sdk_system_get_sdk_version());
	xTaskCreate(mpu9250_read_acc, "mpu_read", 256, NULL, 2, NULL);
}
